var btnGenerateSquare = document.querySelector('.btn-square');
var squaresElement = document.querySelector('.squares');

console.log(btnGenerateSquare);
console.log(squaresElement);

function generateSquare() {
  var square = document.createElement('div');
  square.classList.add('square'); // adicionar classe
  square.style.width = "100px";
  square.style.height = "100px";
  square.style.backgroundColor = "red";
  square.setAttribute("onmouseover", "changeColor(this)");

  squaresElement.append(square);
}

btnGenerateSquare.onclick = generateSquare;


function getRandomColor() {  
  var letters = "0123456789ABCDEF";  
  var color = "#";  
  for (var i = 0; i < 6; i++) {    
    color += letters[Math.floor(Math.random() * 16)];  
  }  
  
  return color;
}

function changeColor(square) {
  square.style.backgroundColor = getRandomColor();
}