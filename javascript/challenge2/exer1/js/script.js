var btnGenerateSquare = document.querySelector('.btn-square');
var squaresElement = document.querySelector('.squares');

console.log(btnGenerateSquare);
console.log(squaresElement);

function generateSquare() {
  var square = document.createElement('div');
  square.classList.add('square'); // adicionar classe
  square.style.width = "100px";
  square.style.height = "100px";
  square.style.backgroundColor = "red";

  squaresElement.append(square);
}

btnGenerateSquare.onclick = generateSquare;