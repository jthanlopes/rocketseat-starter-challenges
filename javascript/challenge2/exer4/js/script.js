var names = ["Diego", "Gabriel", "Lucas"];
var listElement = document.querySelector('.list');

function addIten(name) {
  var listIten = document.createElement('li');
  listIten.innerHTML = name;

  listElement.append(listIten);
}

for (name of names) {
  addIten(name);
}

function add() {
  var inputElement = document.querySelector('.container input');
  addIten(inputElement.value);

  inputElement.value = ''; // limpar input
}