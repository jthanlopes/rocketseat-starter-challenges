var names = ["Diego", "Gabriel", "Lucas"];
var listElement = document.querySelector('.list');

function addIten(name) {
  var listIten = document.createElement('li');
  listIten.innerHTML = name;

  listElement.append(listIten);
}

for (name of names) {
  addIten(name);
}