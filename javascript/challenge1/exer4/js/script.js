// De 0-1 ano: Iniciante
// De 1-3 anos: Intermediário
// De 3-6 anos: Avançado
// De 7 acima: Jedi Master
function getExperience(years) {
  if (years < 2) {
    return "Iniciante";
  } else if (years < 4) {
    return "Intermediário";
  } else if (years < 7) {
    return "Avançado";
  } else {
    return "Jedi Master";
  }

  return 0;
}

var yearsStudy = 7;
console.log(getExperience(yearsStudy));