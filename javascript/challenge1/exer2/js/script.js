function even(x, y) {
  var evens = [];

  for (var i = x; i <= y; i++) {
    if (i%2 == 0) evens.push(i);
  }

  return "(" + evens.join(', ') + ")";
}

// function even(x, y) {
//   for (var i = x; i <= y; i++) {
//     if (i%2 == 0) console.log(i);
//   }
//  }

 console.log(even(32, 321));